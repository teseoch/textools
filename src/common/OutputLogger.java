package common;

public class OutputLogger {
	private StringBuffer log;
	private static OutputLogger instance=null;
	
	public static OutputLogger Instance()
	{
		if(instance==null)
			instance=new OutputLogger();
		
		return instance;
	}
	
	
	private OutputLogger()
	{
		log=new StringBuffer();
	}
	
	public void logMessage(String msg)
	{
		log.append("- [Message] "+msg+"\n");
	}
	
	public void logError(String msg)
	{
		log.append("- [Error] "+msg+"\n");
	}
	
	public void logWarning(String msg)
	{
		log.append("- [Warning] "+msg+"\n");
	}
	
	public void clear()
	{
		log=new StringBuffer();
	}
	
	@Override
	public String toString()
	{
		return log.toString();
	}
}
