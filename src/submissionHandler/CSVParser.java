package submissionHandler;

import java.io.BufferedReader;
import java.io.File;

import java.io.FileReader;
import java.io.IOException;

public class CSVParser {

	public static void Parse(File file, String x, String y, String separator, StringBuffer outX, StringBuffer outY) throws IOException
	{
		String sep=";";
		if(separator.equals("colon"))
			sep=";";
		
		final BufferedReader br = new BufferedReader(new FileReader(file));
		String line;
		
		boolean header=true;
		
		int indexX = -1, indexY = -1;
		
		while ((line = br.readLine()) != null)
		{
			final String trimmed=line.trim();
			String tmp[]=trimmed.split(sep);
			
			if(header)
			{
				for(int i=0;i<tmp.length;++i)
				{
					if(tmp[i].trim().equals(x))
						indexX=i;
					else if(tmp[i].trim().equals(y))
						indexY=i;
				}
				
				header=false;
				continue;
			}
			
			outX.append(tmp[indexX].trim());
			outX.append(" ");
			outY.append(tmp[indexY].trim());
			outY.append(" ");
			
		}
		
		br.close();
	}
}
