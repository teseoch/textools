package submissionHandler;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.CopyOption;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Pattern;

import common.OutputLogger;

public class TexFileParser {
	private final OutputLogger logger;

	private File dir;
	private String outFigDir;
	private String outDataDir;

	private boolean containsPlots;

	private final List<List<File>> figures;
	private final Set<File> dataSet;
	private final List<File> datas;
	private final List<String> dataNames;

	private StringBuffer buffer;


	public TexFileParser(String figDir, String dataDir)
	{ 
		logger = OutputLogger.Instance();

		figures=new ArrayList<>();

		dataSet=new HashSet<>();
		datas=new ArrayList<>();
		dataNames=new ArrayList<>();

		outFigDir = figDir+(figDir.endsWith("/")?"":"/");
		outDataDir = dataDir+(dataDir.endsWith("/")?"":"/");
	}

	public void read(final String path)
	{
		try
		{
			logger.logMessage("reading "+path);
			final File file=new File(path);
			dir=new File(file.getParent());
			buffer=new StringBuffer();
			figures.clear();

			datas.clear();
			dataSet.clear();
			dataNames.clear();

			containsPlots=false;

			readAux(file.getName());
		}
		catch(final IOException e)
		{
			logger.logError(e.getMessage());
		}

	}

	public void save(final String path)
	{
		try
		{
			final PrintWriter pw = new PrintWriter(path);
			pw.write(buffer.toString());
			pw.close();
			logger.logMessage("output written to "+path);

			final String root=new File(path).getParent();

			final int nImages = copyImages(root);
			logger.logMessage(nImages+" images copied to "+outFigDir);

			copyData(root);
			logger.logMessage(datas.size()+" data copied to "+outDataDir);


			if(containsPlots)
			{
				logger.logWarning("add to your preamble\n"+
						"\t\t\\usepgfplotslibrary{external}\n"+
						"\t\t\\tikzexternalize[prefix=<folder>]\n"+
						"\tand complile with 'pdflatex -shell-escape'");
			}
			
			logger.logWarning("remember to copy your bibs files and styles");
		}
		catch(final IOException e)
		{
			logger.logError(e.getMessage());
		}
	}




	private int copyImages(final String dirPath) throws IOException
	{
		if(figures.isEmpty()) return 0;
		
		int total=0;
		final File dir=new File(dirPath);
		final File out=new File(dir, outFigDir);

		if(!out.isDirectory())
			out.mkdir();

		for(int i=0;i<figures.size();++i)
		{
			final String mainName=(i+1)+"";
			final List<File> currentFigure=figures.get(i);

			for(int j=0;j<currentFigure.size();++j)
			{

				final char localName=(char) ('a'+j);
				File oldFile=currentFigure.get(j);
				String fileName=oldFile.getName();
				final String tmpName=fileName;

				if(!oldFile.exists())
				{
					final File currentDir=oldFile.getParentFile();

					final String[] candidates=currentDir.list(new FilenameFilter() {
						@Override
						public boolean accept(final File dir, final String name) {
							return name.contains(tmpName);
						}
					});

					int minExt=10000;
					fileName="";

					for(final String s : candidates)
					{
						final String ext=getExtension(s);
						int currentExt=10000;

						if(ext.equals("eps")) currentExt=0;
						else if(ext.equals("png")) currentExt=1;
						else if(ext.equals("jpg")) currentExt=2;
						else if(ext.equals("jpeg")) currentExt=3;
						else if(ext.equals("pdf")) currentExt=9998;
						else
						{
							logger.logError("unsupported extension "+ext+", try eps, png, jpg, jpeg or pdf");
							currentExt=9999;
						}

						if(currentExt<minExt)
						{
							minExt=currentExt;
							fileName=s;
						}
					}

					oldFile=new File(currentDir,fileName);
				}


				final File newFile=new File(out, mainName+localName+"."+getExtension(fileName));

				final CopyOption[] options = new CopyOption[]{
						StandardCopyOption.REPLACE_EXISTING,
						StandardCopyOption.COPY_ATTRIBUTES
				}; 
				Files.copy(oldFile.toPath(), newFile.toPath(), options);
				++total;

			}
		}

		return total;
	}

	private void copyData(final String dirPath) throws IOException
	{
		if(datas.isEmpty()) return;
		
		final File dir=new File(dirPath);
		final File out=new File(dir, outDataDir);

		if(!out.isDirectory())
			out.mkdir();

		for(int i=0;i<datas.size();++i)
		{
			final File oldFile=datas.get(i);
			final String newName=dataNames.get(i);
			final String fileName=oldFile.getName();

			if(!oldFile.exists())
			{
				logger.logError("unable to find " + oldFile);
				continue;
			}


			final File newFile=new File(out, newName+"."+getExtension(fileName));

			final CopyOption[] options = new CopyOption[]{
					StandardCopyOption.REPLACE_EXISTING,
					StandardCopyOption.COPY_ATTRIBUTES
			}; 
			Files.copy(oldFile.toPath(), newFile.toPath(), options);

		}

	}


	private String getExtension(final String name)
	{
		String ext = "";
		final int index = name.lastIndexOf('.');
		if (index >= 0) {
			ext = name.substring(index+1);
		}

		return ext;
	}

	private void readAux(final String name) throws IOException
	{
		final File file=new File(dir, name);
		final BufferedReader br = new BufferedReader(new FileReader(file));
		String line;
		List<File> currentFigure=null;
		String mainName=null;
		boolean hasPlot=false;


		while ((line = br.readLine()) != null)
		{
			final String trimmed=line.trim();

			if(trimmed.contains("\\input"))
			{
				String newName=line.replaceAll(Pattern.quote("\\input{"), "").replaceAll(Pattern.quote("}"), "").trim();
				buffer.append("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n");
				buffer.append("%%%%%%%%%%%%%%%%%%% - "+newName+" - %%%%%%%%%%%%%%%%%%%%%%%\n");
				buffer.append("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n");

				if(!newName.endsWith(".tex"))
					newName+=".tex";

				readAux(newName);
				buffer.append("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n");
				buffer.append("\n\n");

				continue;
			}

			if(trimmed.contains("\\begin{tikzpicture}"))
				hasPlot=true;



			if(hasPlot)
			{
				final List<File> tmp=new ArrayList<>();
				final String prefix=(datas.size()+1)+"";
				line = extractImages("table[", line, tmp, outDataDir, prefix);
				containsPlots=true;

				int index=0;
				for(int i=0;i<tmp.size();++i)
				{
					final File f=tmp.get(i);
					final char localName=(char) ('a'+i);
					final String oldName=prefix+localName;
					String newName;

					if(dataSet.add(f))
					{
						datas.add(f);
						newName=prefix+(char)('a'+index);
						dataNames.add(newName);


						++index;
					}
					else
					{
						final int oldIndex = datas.indexOf(f);
						newName=dataNames.get(oldIndex);
					}

					line=line.replaceAll(oldName, newName);
				}

				if(trimmed.contains("\\end{tikzpicture}")){
					hasPlot=false;
				}



				buffer.append(line+"\n");

				continue;
			}


			if(trimmed.contains("\\begin{figure}"))
			{
				currentFigure=new ArrayList<>();
				figures.add(currentFigure);
				mainName=figures.size()+"";
			}


			if(!trimmed.startsWith("%")){
				line = extractImages("\\includegraphics", line, currentFigure, outFigDir, mainName);
				line = extractImages("\\begin{overpic}",  line, currentFigure, outFigDir, mainName);
			}


			buffer.append(line+"\n");

		}
		br.close();
	}


	private String extractImages(final String token, String line, final List<File> currentFigure, final String outDir, final String prefix) {
		if(line.contains(token))
		{
			int incIndex=line.indexOf(token, 0)+token.length();

			while(incIndex>=0)
			{
				incIndex+=token.length();
				final int fromIndex=line.indexOf('{', incIndex);
				final int toIndex=line.indexOf('}', incIndex);

				char localName=(char) ('a'+currentFigure.size());
				final String fileName=line.substring(fromIndex+1, toIndex);

				String ext=getExtension(fileName);
				if(!ext.isEmpty())
					ext="."+ext;

				final File fig=new File(dir,fileName);
				if(!currentFigure.contains(fig))
					currentFigure.add(fig);
				else
					localName=(char) ('a'+currentFigure.indexOf(fig));

				line=line.substring(0,fromIndex+1)+outDir+prefix+localName+ext+line.substring(toIndex, line.length());



				incIndex=line.indexOf(token, incIndex);
			}
		}
		return line;
	}


	//	private void processPlot(final String plotCode) throws IOException {
	//		final String name="test";
	//
	//		final StringBuffer matlab = new StringBuffer();
	//
	//		matlab.append("function asd\n\n");
	//
	//		final StringBuffer plots = new StringBuffer();
	//		final Pattern p = Pattern.compile("\\\\begin\\{\\w*axis\\}");
	//
	//		final Matcher m = p.matcher(plotCode);
	//
	//		boolean logX=false, logY=false;
	//
	//		int currentPos=-1;
	//		if (m.find()) {
	//			currentPos= m.end();
	//
	//			final String axis=m.group();
	//
	//			if(axis.contains("loglog"))
	//			{
	//				logX=true;
	//				logY=true;
	//			}
	//			else if(axis.contains("semilogx"))
	//				logX=true;
	//			else if(axis.contains("semilogy"))
	//				logY=true;
	//		}
	//
	//		currentPos=plotCode.indexOf('[',currentPos)+1;
	//		int next=plotCode.indexOf(']',currentPos);
	//
	//		final String axisOpt=plotCode.substring(currentPos,next).trim();
	//		parseAxisOpts(axisOpt,matlab);
	//
	//		if(logX)
	//			matlab.append("axisData.xlog=true;\n");
	//		if(logY)
	//			matlab.append("axisData.ylog=true;\n\n");
	//
	//
	//
	//		currentPos=next;
	//
	//		int nPlots=0;
	//		while(currentPos>=0 && currentPos<=plotCode.length()){
	//			++nPlots;
	//			currentPos=plotCode.indexOf("\\addplot", currentPos);
	//
	//			if(currentPos<0) break;
	//
	//			next=plotCode.indexOf(";", currentPos);
	//
	//			parsePlot(plotCode.substring(currentPos,next).trim(), plots,nPlots);
	//			++currentPos;
	//		}
	//
	//		currentPos=next;
	//
	//		matlab.append("datas=cell("+(nPlots-1)+",1);\n\n");
	//		matlab.append(plots);
	//
	//		matlab.append("\n\n\n\n\n");
	//
	//
	//
	//		matlab.append("minX=min(datas{1}.x);\n");
	//		matlab.append("minY=min(datas{1}.y);\n");
	//		matlab.append("maxX=max(datas{1}.x);\n");
	//		matlab.append("maxY=max(datas{1}.y);\n\n");
	//
	//		matlab.append("for i=1:length(datas)\n");
	//		matlab.append("\tminX=min(minX,min(datas{i}.x));\n");
	//		matlab.append("\tminY=min(minY,min(datas{i}.y));\n");
	//		matlab.append("\tmaxX=max(maxX,max(datas{i}.x));\n");
	//		matlab.append("\tmaxY=max(maxY,max(datas{i}.y));\n");
	//		matlab.append("end\n");
	//
	//		matlab.append("\n\n\n\n\n");
	//
	//
	//		matlab.append("if ~isfield(axisData,'ticksy')\n");
	//		matlab.append("\taxisData.ticksy=linspace(floor(minY),floor(maxY),5);\n");
	//		matlab.append("end\n\n");
	//		matlab.append("if ~isfield(axisData,'ticksx')\n");
	//		matlab.append("\taxisData.ticksx=linspace(floor(minX),ceil(maxX),5);\n");
	//		matlab.append("end\n\n\n");
	//
	//		matlab.append("axisData.axis=[min(minX, min(axisData.ticksx))-0.05 max(maxX, max(axisData.ticksx))+0.05 min(minY, min(axisData.ticksy))-0.05  max(maxY, max(axisData.ticksy))+0.05];\n\n");
	//
	//		matlab.append("opts.scalingx = opts.scalingx/(maxX-minX);\n");
	//		matlab.append("opts.scalingy = opts.scalingy/(maxY-minY);\n\n");
	//
	//
	//
	//		matlab.append("lblTicksX=cell(size(axisData.ticksx));\n");
	//		matlab.append("for i=1:length(axisData.ticksx)\n");
	//		matlab.append("\tlblTicksX{i}=sprintf('%.1f',axisData.ticksx(i));\n");
	//		matlab.append("end\n\n");
	//
	//		matlab.append("lblTicksY=cell(size(axisData.ticksy));\n");
	//		matlab.append("for i=1:length(axisData.ticksy)\n");
	//		matlab.append("\tlblTicksY{i}=sprintf('%.1f',axisData.ticksy(i) );\n");
	//		matlab.append("end\n\n");
	//
	//		matlab.append("axisData.lblTicksX=lblTicksX;\n");
	//		matlab.append("axisData.lblTicksY=lblTicksY;\n");
	//
	//
	//		matlab.append("svgPlot('"+name+".svg',datas,axisData,legendData,opts);");
	//		System.out.println(matlab);
	//	}
	//
	//
	//	private void parsePlot(final String plotCode, final StringBuffer plot, final int index) throws IOException
	//	{
	//		plot.append("data={};\n");
	//
	//		int start=plotCode.indexOf('[')+1;
	//		int currentIndex=plotCode.indexOf(']',start);
	//
	//		final String opts=plotCode.substring(start, currentIndex).trim(); //TODO
	//		plot.append("data.lineStyle='stroke=\"black\" stroke-width=\"1\"';\n");
	//		plot.append("data.markerStyle='stroke=\"black\" stroke-width=\"0\" fill=\"black\"';\n");
	//		plot.append("data.addDots=true;\n");
	//		plot.append("data.name='plot"+index+"';\n");
	//
	//
	//		start=plotCode.indexOf("table",currentIndex);
	//		boolean noCSV=false;
	//		if(start<0)
	//		{
	//
	//			noCSV=true;
	//		}
	//
	//		++start;
	//		start=plotCode.indexOf('[',start)+1;
	//		currentIndex=plotCode.indexOf(']',start);
	//
	//		final String tableOpts=plotCode.substring(start, currentIndex).trim();
	//
	//
	//		final String tmp[]=tableOpts.split(",");
	//		String x="", y="", sep="";
	//		for(final String s : tmp)
	//		{
	//			final String tmp1[]=s.trim().split("=");
	//			if(tmp1.length!=2) continue;
	//
	//			if(tmp1[0].trim().equals("x"))
	//				x=tmp1[1].trim();
	//			else if(tmp1[0].trim().equals("y"))
	//				y=tmp1[1].trim();
	//			else if(tmp1[0].trim().equals("col sep"))
	//				sep=tmp1[1].trim();
	//		}
	//
	//		start=plotCode.indexOf("{",currentIndex)+1;
	//		currentIndex=plotCode.indexOf('}',start);
	//		String path=plotCode.substring(start, currentIndex).trim();
	//
	//
	//		if(noCSV)
	//		{
	//			path=path.replaceAll(Pattern.quote("*"), ".*").replaceAll(Pattern.quote("^"), ".^");
	//			System.out.println(path);
	//
	//			String n="10";
	//			String min="0", max="1";
	//			for(final String s : tmp)
	//			{
	//				final String tmp1[]=s.trim().split("=");
	//				if(tmp1.length!=2) continue;
	//
	//				if(tmp1[0].trim().equals("domain"))
	//				{
	//					final String tmp2[] = tmp1[1].trim().split(":");
	//					min=tmp2[0].trim();
	//					max=tmp2[1].trim();
	//				}
	//				else if(tmp1[0].trim().equals("samples"))
	//					n=tmp1[1].trim();
	//			}
	//
	//			plot.append("data.x=linspace("+min+","+max+","+n+");\n");
	//			plot.append("func_"+index+"=@(x)("+path+");\n");
	//			plot.append("data.y=func_"+index+"(data.x);\n");
	//		}
	//		else
	//		{
	//			final StringBuffer buffx = new StringBuffer();
	//			final StringBuffer buffy = new StringBuffer();
	//
	//			buffx.append("[");
	//			buffy.append("[");
	//			CSVParser.Parse(new File(dir,path), x, y, sep, buffx, buffy);
	//
	//			buffx.append("];\n");
	//			buffy.append("];\n");
	//
	//			plot.append("data.x=");
	//			plot.append(buffx);
	//
	//			plot.append("data.y=");
	//			plot.append(buffy);
	//
	//		}
	//		plot.append("datas{"+index+"}=data;\n");
	//
	//		plot.append("\n\n\n");
	//	}
	//
	//	private void parseAxisOpts(final String axisOpt, final StringBuffer matlab)
	//	{
	//		final double lineWidth = 200;
	//		final double lineHeight =40;
	//
	//		matlab.append("axisData={};\n");
	//		matlab.append("legendData={};\n");
	//		matlab.append("opts={};\n");
	//
	//		int current=0;
	//
	//		double w=lineWidth,h=lineHeight;
	//
	//
	//		while(current>=0 && current<=axisOpt.length())
	//		{
	//			final int next=axisOpt.indexOf('=',current)+1;
	//			final String key=axisOpt.substring(current,next-1).trim();
	//
	//			current=axisOpt.indexOf(',',next);
	//			final int opend=axisOpt.indexOf('{',next);
	//
	//			if(opend>=0 && opend<current)
	//				current=axisOpt.indexOf('}',next)+1;
	//
	//			if(current<0) current=axisOpt.length();
	//
	//			final String value=axisOpt.substring(next,current).trim();
	//
	//			if(key.equalsIgnoreCase("width"))
	//			{
	//				final String vals=value.replaceAll(Pattern.quote("\\pgfplotswidth"), "").replaceAll(Pattern.quote("\\linewidth"), "").trim();
	//				if(!vals.isEmpty()){
	//					final double val=Double.parseDouble(vals);
	//					w=lineWidth*val;
	//				}
	//			}
	//			else if(key.equalsIgnoreCase("height"))
	//			{
	//				final String vals=value.replaceAll(Pattern.quote("\\pgfplotswidth"), "").replaceAll(Pattern.quote("\\linewidth"), "");
	//				if(!vals.isEmpty()){
	//					final double val=Double.parseDouble(vals);
	//					h=lineHeight*val;
	//				}
	//			}
	//			else if(key.equalsIgnoreCase("xlabel"))
	//			{
	//				matlab.append("axisData.xlabel="+value.replaceAll("\\{", "'").replaceAll("\\}", "'")+";\n");
	//			}
	//			else if(key.equalsIgnoreCase("ylabel"))
	//			{
	//				matlab.append("axisData.ylabel="+value.replaceAll("\\{", "'").replaceAll("\\}", "'")+";\n");
	//			}
	//
	//			else if(key.equalsIgnoreCase("xtick"))
	//			{
	//				matlab.append("axisData.ticksx="+value.replaceAll("\\{", "[").replaceAll("\\}", "]")+";\n");
	//			}
	//			else
	//			{
	//				System.out.println("not processing "+key+" "+value);
	//			}
	//
	//			matlab.append("\n");
	//			++current;
	//
	//		}
	//
	//
	//		matlab.append("opts.scalingx="+w+";\n");
	//		matlab.append("opts.scalingy="+h+";\n");
	//
	//
	//		matlab.append("\n\n\n");
	//
	//
	//
	//	}

}
