package submissionHandler;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import common.OutputLogDialog;
import common.OutputLogger;

import java.awt.GridBagLayout;
import javax.swing.JButton;
import javax.swing.JFileChooser;

import java.awt.FlowLayout;
import javax.swing.JLabel;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.event.ActionListener;
import java.io.File;
import java.awt.event.ActionEvent;
import javax.swing.JTextField;

public class SubmissionHandlerMain extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private final JPanel contentPane;
	private final JTextField inputFile;
	private final JTextField outputFile;
	private final JTextField outFigDir;
	private final JTextField outDataDir;
	private File lastFile;

	/**
	 * Launch the application.
	 */
	public static void main(final String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					final SubmissionHandlerMain frame = new SubmissionHandlerMain();
					frame.setVisible(true);
				} catch (final Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public SubmissionHandlerMain() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		lastFile=new File("");

		setTitle("Merge and rename figures for final submission");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 220);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new BorderLayout(0, 0));

		final JPanel panel = new JPanel();
		final FlowLayout flowLayout = (FlowLayout) panel.getLayout();
		flowLayout.setVgap(0);
		flowLayout.setHgap(0);
		flowLayout.setAlignment(FlowLayout.RIGHT);
		contentPane.add(panel, BorderLayout.SOUTH);



		final JPanel panel_1 = new JPanel();
		contentPane.add(panel_1, BorderLayout.CENTER);
		final GridBagLayout gbl_panel_1 = new GridBagLayout();
		gbl_panel_1.columnWidths = new int[]{0, 0, 0, 0};
		gbl_panel_1.rowHeights = new int[]{0, 0, 0, 0, 0};
		gbl_panel_1.columnWeights = new double[]{0.0, 1.0, 0.0, Double.MIN_VALUE};
		gbl_panel_1.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		panel_1.setLayout(gbl_panel_1);

		final JLabel lblInputFile = new JLabel("Input file");
		final GridBagConstraints gbc_lblInputFile = new GridBagConstraints();
		gbc_lblInputFile.anchor = GridBagConstraints.EAST;
		gbc_lblInputFile.insets = new Insets(0, 0, 5, 5);
		gbc_lblInputFile.gridx = 0;
		gbc_lblInputFile.gridy = 0;
		panel_1.add(lblInputFile, gbc_lblInputFile);

		final JButton browseInFile = new JButton("Browse...");
		browseInFile.addActionListener(new ActionListener() {
			public void actionPerformed(final ActionEvent e) {
				final JFileChooser fc=new JFileChooser();
				fc.setCurrentDirectory(lastFile);
				final int val=fc.showOpenDialog(SubmissionHandlerMain.this);

				if(val==JFileChooser.APPROVE_OPTION){
					final File f=fc.getSelectedFile();
					lastFile=f.getParentFile();
					inputFile.setText(f.getAbsolutePath());
				}
			}
		});

		inputFile = new JTextField();
		final GridBagConstraints gbc_inputFile = new GridBagConstraints();
		gbc_inputFile.insets = new Insets(0, 0, 5, 5);
		gbc_inputFile.fill = GridBagConstraints.HORIZONTAL;
		gbc_inputFile.gridx = 1;
		gbc_inputFile.gridy = 0;
		panel_1.add(inputFile, gbc_inputFile);
		inputFile.setColumns(10);
		final GridBagConstraints gbc_browseInFile = new GridBagConstraints();
		gbc_browseInFile.insets = new Insets(0, 0, 5, 0);
		gbc_browseInFile.gridx = 2;
		gbc_browseInFile.gridy = 0;
		panel_1.add(browseInFile, gbc_browseInFile);

		final JLabel lblOutputFile = new JLabel("Output file");
		final GridBagConstraints gbc_lblOutputFile = new GridBagConstraints();
		gbc_lblOutputFile.anchor = GridBagConstraints.EAST;
		gbc_lblOutputFile.insets = new Insets(0, 0, 5, 5);
		gbc_lblOutputFile.gridx = 0;
		gbc_lblOutputFile.gridy = 1;
		panel_1.add(lblOutputFile, gbc_lblOutputFile);

		outputFile = new JTextField();
		final GridBagConstraints gbc_outputFile = new GridBagConstraints();
		gbc_outputFile.insets = new Insets(0, 0, 5, 5);
		gbc_outputFile.fill = GridBagConstraints.HORIZONTAL;
		gbc_outputFile.gridx = 1;
		gbc_outputFile.gridy = 1;
		panel_1.add(outputFile, gbc_outputFile);
		outputFile.setColumns(10);

		final JButton browseOutFile = new JButton("Browse...");
		browseOutFile.addActionListener(new ActionListener() {
			public void actionPerformed(final ActionEvent e) {
				final JFileChooser fc=new JFileChooser();
				fc.setCurrentDirectory(lastFile);
				final int val=fc.showSaveDialog(SubmissionHandlerMain.this);

				if(val==JFileChooser.APPROVE_OPTION){
					final File f=fc.getSelectedFile();
					lastFile=f.getParentFile();
					outputFile.setText(f.getAbsolutePath());

				}
			}
		});
		final GridBagConstraints gbc_browseOutFile = new GridBagConstraints();
		gbc_browseOutFile.insets = new Insets(0, 0, 5, 0);
		gbc_browseOutFile.gridx = 2;
		gbc_browseOutFile.gridy = 1;
		panel_1.add(browseOutFile, gbc_browseOutFile);

		final JLabel lblOutputFigureDirectory = new JLabel("Output figure directory");
		final GridBagConstraints gbc_lblOutputFigureDirectory = new GridBagConstraints();
		gbc_lblOutputFigureDirectory.anchor = GridBagConstraints.EAST;
		gbc_lblOutputFigureDirectory.insets = new Insets(0, 0, 5, 5);
		gbc_lblOutputFigureDirectory.gridx = 0;
		gbc_lblOutputFigureDirectory.gridy = 2;
		panel_1.add(lblOutputFigureDirectory, gbc_lblOutputFigureDirectory);

		outFigDir = new JTextField();
		final GridBagConstraints gbc_outFigDir = new GridBagConstraints();
		gbc_outFigDir.insets = new Insets(0, 0, 5, 5);
		gbc_outFigDir.fill = GridBagConstraints.HORIZONTAL;
		gbc_outFigDir.gridx = 1;
		gbc_outFigDir.gridy = 2;
		panel_1.add(outFigDir, gbc_outFigDir);
		outFigDir.setColumns(10);

		final JLabel lblOutputDataDirectory = new JLabel("Output data directory");
		final GridBagConstraints gbc_lblOutputDataDirectory = new GridBagConstraints();
		gbc_lblOutputDataDirectory.insets = new Insets(0, 0, 0, 5);
		gbc_lblOutputDataDirectory.anchor = GridBagConstraints.EAST;
		gbc_lblOutputDataDirectory.gridx = 0;
		gbc_lblOutputDataDirectory.gridy = 3;
		panel_1.add(lblOutputDataDirectory, gbc_lblOutputDataDirectory);

		outDataDir = new JTextField();
		final GridBagConstraints gbc_outDataDir = new GridBagConstraints();
		gbc_outDataDir.insets = new Insets(0, 0, 0, 5);
		gbc_outDataDir.fill = GridBagConstraints.HORIZONTAL;
		gbc_outDataDir.gridx = 1;
		gbc_outDataDir.gridy = 3;
		panel_1.add(outDataDir, gbc_outDataDir);
		outDataDir.setColumns(10);


		final JButton btnCancel = new JButton("Cancel");
		btnCancel.addActionListener(new ActionListener() {
			public void actionPerformed(final ActionEvent e) {
				System.exit(0);
			}
		});
		panel.add(btnCancel);

		final JButton btnOk = new JButton("Ok");
		btnOk.addActionListener(new ActionListener() {
			public void actionPerformed(final ActionEvent e) {
				OutputLogger.Instance().clear();
				OutputLogger.Instance().logMessage("Started...");
				final TexFileParser p=new TexFileParser(outFigDir.getText(),outDataDir.getText());

				p.read(inputFile.getText());
				p.save(outputFile.getText());

				OutputLogger.Instance().logMessage("Merge finished!");


				new OutputLogDialog(SubmissionHandlerMain.this);
			}
		});
		panel.add(btnOk);
	}

}
