package bibMerger;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Pattern;

import common.OutputLogger;

public class Merger {

	public static BibDatabase Merge(final ArrayList<BibDatabase> toMerge, final List<EntryKey> toSkip, final boolean autogenerateIDS, final String bblPath)
	{
		final BibDatabase res=new BibDatabase();

		final ArrayList<String> keys=new ArrayList<String>();

		int totalEntries=0;

		Set<String> valid=null;
		if(bblPath!=null && !bblPath.isEmpty())
		{
			try
			{
				valid=new HashSet<>();
				final File file=new File(bblPath);
				final BufferedReader br = new BufferedReader(new FileReader(file));
				String line;
				while ((line = br.readLine()) != null) {
					line=line.trim();
					if(line.contains("\\bibitem{"))
					{
						line=line.replaceAll(Pattern.quote("\\bibitem{"), "").replaceAll(Pattern.quote("}"), "");
						valid.add(line);
					}
				}

				br.close();
			} catch (final IOException e) {
				valid=null;
				OutputLogger.Instance().logError("unable the read bbl file");
			}
		}

		for(final BibDatabase d : toMerge){
			if(autogenerateIDS)
				d.autogenerateID();

			totalEntries+=d.keys().size();
			keys.addAll(d.keys());
		}

		Collections.sort(keys, new Comparator<String>() {
			@Override
			public int compare(final String s1, final String s2) {
				return s1.toLowerCase().compareTo(s2.toLowerCase());
			}
		});

		String prev="";

		for(final String k : keys)
		{
			if(k.equals(prev)) continue;

			if(valid!=null && !valid.isEmpty() && !valid.contains(k)) continue;

			BibEntry tmp=null;

			for(final BibDatabase d : toMerge)
			{
				tmp=d.get(k);

				if(tmp!=null) break;
			}

			if(tmp!=null){
				tmp.setEntriesToSkip(toSkip);
				res.insert(k, tmp);
			}

			prev=k;
		}

		OutputLogger.Instance().logMessage("Created " + res.keys().size()+"/"+totalEntries+" entries");

		return res;

	}
}
