package bibMerger;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;

import common.OutputLogger;

public class JournalEntryDB {
	private final Map<String,String> abbr2Name, name2Abbr;
	private final Set<String> missingJournals;

	public JournalEntryDB()
	{
		abbr2Name=new HashMap<>();
		name2Abbr=new HashMap<>();
		missingJournals=new HashSet<>();
	}

	public void addFromFile(final String path)
	{
		try{
			final File file=new File(path);
			final BufferedReader br = new BufferedReader(new FileReader(file));
			String line;
			while ((line = br.readLine()) != null){
				line=line.trim();
				if(line.isEmpty() || line.startsWith("%") || !line.startsWith("@")) continue;

				line=line.replaceAll(Pattern.quote("@string"), "");
				line=line.replaceAll(Pattern.quote("@STRING"), "");
				line=line.replaceAll(Pattern.quote("{"), "").replaceAll(Pattern.quote("}"), "");
				final String[] tmp=line.split("=");

				if(tmp.length!=2)
				{
					OutputLogger.Instance().logWarning("[Warning] invalid line "+line);
					continue;
				}

				final String abbr=tmp[0].trim();
				String name=tmp[1].trim();//.replaceAll(Pattern.quote("\\"), "");

				if(name.startsWith("\"") && name.endsWith("\""))
					name=name.substring(1, name.length()-1);

				abbr2Name.put(abbr, name.trim());
				name2Abbr.put(name.toLowerCase(), abbr);
			}

			br.close();
		} catch (final IOException e) {
			OutputLogger.Instance().logError(e.getMessage());
		}

	}

	public String abbrFor(final String in) {

		final String value=in.replaceAll(Pattern.quote("{"), "").replaceAll(Pattern.quote("}"), "").trim();
		return name2Abbr.get(value.toLowerCase());
	}

	public boolean hasAbbr(final String in) {
		final String value=in.replaceAll(Pattern.quote("{"), "").replaceAll(Pattern.quote("}"), "").trim();
		return abbr2Name.containsKey(value);
	}

	public void logMissingJournals()
	{
		for(final String value : missingJournals)
			OutputLogger.Instance().logWarning("journal entry for "+value+" not found");
	}

	public void addMissingJournal(final String value) {
		missingJournals.add(value);
	}
}
