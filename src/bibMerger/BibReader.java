package bibMerger;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Set;

public class BibReader {

	public static BibDatabase ReadBibFile(final String path, final JournalEntryDB journalDB, Set<EntryKey> entries) throws IOException
	{
		final BibDatabase result=new BibDatabase();

		final File file=new File(path);
		final StringBuffer buffer = new StringBuffer();
		final BufferedReader br = new BufferedReader(new FileReader(file));
		String line;
		while ((line = br.readLine()) != null)
			buffer.append(line+"\n");
		br.close();


		final String[] items=buffer.toString().split("@");

		for(int i=0;i<items.length;++i)
		{
			String tmp=items[i];
			
			for(int j=i+1;j<items.length;++j)
			{
				if(items[j].toLowerCase().startsWith("article") ||
						items[j].toLowerCase().startsWith("book") ||
						items[j].toLowerCase().startsWith("booklet") ||
						items[j].toLowerCase().startsWith("conference") ||
						items[j].toLowerCase().startsWith("inbook") ||
						items[j].toLowerCase().startsWith("incollection") ||
						items[j].toLowerCase().startsWith("inproceedings") ||
						items[j].toLowerCase().startsWith("manual") ||
						items[j].toLowerCase().startsWith("mastersthesis") ||
						items[j].toLowerCase().startsWith("misc") ||
						items[j].toLowerCase().startsWith("phdthesis") ||
						items[j].toLowerCase().startsWith("proceedings") ||
						items[j].toLowerCase().startsWith("techreport") ||
						items[j].toLowerCase().startsWith("unpublished") 
						)
					break;
				
				++i;
				tmp+="@"+items[j];
			}
			
			tmp.trim();

			if(tmp.isEmpty()) continue;

			result.parseAndInsert(tmp,journalDB,entries);
		}

		return result;

	}
}
