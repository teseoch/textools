package bibMerger;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Set;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.EmptyBorder;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JLabel;

public class EntriesSelection extends JDialog {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private final JPanel contentPanel = new JPanel();
	private ArrayList<EntryKey> entries;
	private ArrayList<JCheckBox> boxes;

	/**
	 * Create the dialog.
	 */
	public EntriesSelection(Frame parent, Set<EntryKey> entriesIn) {
		super(parent);
		this.entries=new ArrayList<>(entriesIn);
		boxes=new ArrayList<>();
		Collections.sort(this.entries);
		
		setBounds(100, 100, 450, 384);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(new BorderLayout(0, 0));
		JPanel main=new JPanel(new GridLayout((int)Math.ceil(entries.size()/3.0), 3, 0, 0));
		
		
		contentPanel.add(new JScrollPane(main),BorderLayout.CENTER);
		{
			JLabel lblSelectDesiredEntries = new JLabel("Select desired entries");
			contentPanel.add(lblSelectDesiredEntries, BorderLayout.NORTH);
		}
		{
			JPanel panel = new JPanel();
			contentPanel.add(panel, BorderLayout.SOUTH);
			{
				JButton btnDeselect = new JButton("Deselect");
				btnDeselect.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						for(JCheckBox cb : boxes)
							cb.setSelected(false);
					}
				});
				panel.add(btnDeselect);
			}
			{
				JButton btnSelectAll = new JButton("Select all");
				btnSelectAll.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						for(JCheckBox cb : boxes)
							cb.setSelected(true);
					}
				});
				panel.add(btnSelectAll);
			}
			{
				JButton btnSelectRelevant = new JButton("Select relevant");
				btnSelectRelevant.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						for(int i=0;i<boxes.size();++i)
							boxes.get(i).setSelected(entries.get(i).priority>=0);
					}
				});
				panel.add(btnSelectRelevant);
			}
		}
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("OK");
				okButton.setActionCommand("OK");
				okButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						for(int i=0;i<boxes.size();++i)
							entries.get(i).enabled=boxes.get(i).isSelected();
						setVisible(false);
					}
				});
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
		}
		
		for(EntryKey e : this.entries)
		{
			JCheckBox cb = new JCheckBox(e.name, true);
			boxes.add(cb);
			main.add(cb);
		}
		

		setModal(true);
		setVisible(true);
	}
	

	public ArrayList<EntryKey> getEntries() {
		return entries;
	}


}
