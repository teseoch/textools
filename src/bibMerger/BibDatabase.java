package bibMerger;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Set;

public class BibDatabase {
	private HashMap<String, BibEntry> db;

	public BibDatabase()
	{
		db=new HashMap<String, BibEntry>();
	}

	public void insert(final String key, final BibEntry entry)
	{
		db.put(key, entry);
	}

	public void parseAndInsert(final String text, final JournalEntryDB journalDB, final Set<EntryKey> entries)
	{
		final BibEntry entry=new BibEntry(text, journalDB, entries);
		insert(entry.getKey(), entry);
	}

	public String toString()
	{
		final StringBuffer res=new StringBuffer();

		final ArrayList<String> keys=new ArrayList<String>(keys());
		Collections.sort(keys, new Comparator<String>() {
			@Override
			public int compare(final String s1, final String s2) {
				return s1.toLowerCase().compareTo(s2.toLowerCase());
			}
		});

		for(final String s : keys){
			res.append(get(s));
			res.append("\n\n");
		}

		return res.toString();
	}

	public Set<String> keys() {
		return db.keySet();
	}

	public BibEntry get(final String k) {
		return db.get(k);
	}

	public void autogenerateID() {
		final HashMap<String, BibEntry> dbtmp = new HashMap<String, BibEntry>();
		for(final String k : db.keySet())
		{
			final BibEntry tmp=db.get(k);
			tmp.autogenerateID();
			dbtmp.put(k,tmp);
		}

		db=dbtmp;
	}
}
