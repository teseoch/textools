package bibMerger;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.border.LineBorder;

import common.OutputLogDialog;
import common.OutputLogger;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;
import java.awt.GridBagLayout;
import javax.swing.JCheckBox;
import java.awt.GridLayout;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JMenu;



/*
 * add possibility to id
 * add checkboxes for avoiding fields
 */
public class BibMergerMain extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private final JPanel contentPane;
	private final JTextField outPath;
	private final JList<String> inputPath;
	private final DefaultListModel<String> listModel;
	private final DefaultListModel<String> listModelDB;
	private final JTextField bblPath;

	/**
	 * Launch the application.
	 */
	public static void main(final String[] args) {
		if(args.length>=1 && (args[0].equals("-h") || args[0].equals("-help")|| args[0].equals("help") || args[0].equals("--help")))
		{
			System.out.println("Usage -i <file1.bib> <file2.bib> ... -db <jlong.bib> <jshort.bib> ... -bbl <bbl file.bbl>");
		}

		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					final BibMergerMain frame = new BibMergerMain(args);
					frame.setVisible(true);
				} catch (final Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public BibMergerMain(final String[] args) {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 400);
		
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		JMenu menu = new JMenu("?");
		menuBar.add(menu);
		
		JMenuItem mntmAbout = new JMenuItem("About");
		mntmAbout.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new AboutDialog(BibMergerMain.this);
			}
		});
		menu.add(mntmAbout);
		contentPane = new JPanel();
		contentPane.setBackground(Color.WHITE);
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);

		final JPanel footer = new JPanel();
		contentPane.add(footer, BorderLayout.SOUTH);
		footer.setLayout(new BorderLayout(0, 0));

		final JPanel browsePanel = new JPanel();
		browsePanel.setBackground(Color.WHITE);
		footer.add(browsePanel, BorderLayout.CENTER);
		browsePanel.setLayout(new GridLayout(2, 1, 0, 0));

		final JCheckBox autogenerateIDS = new JCheckBox("Autogenerate ids");
		browsePanel.add(autogenerateIDS);

		final JPanel panel = new JPanel();
		panel.setBackground(Color.WHITE);
		browsePanel.add(panel);

		final JLabel lblOutput = new JLabel("Output");
		panel.add(lblOutput);

		outPath = new JTextField();//"/Users/teseo/Desktop/asdasd.bib");
		panel.add(outPath);
		outPath.setColumns(20);


		final JButton btnBrowse = new JButton("Browse...");
		panel.add(btnBrowse);
		btnBrowse.addActionListener(new ActionListener() {
			public void actionPerformed(final ActionEvent e) {
				final JFileChooser fc=new JFileChooser();
				final int res=fc.showSaveDialog(BibMergerMain.this);

				if(res==JFileChooser.APPROVE_OPTION){
					final File f=fc.getSelectedFile();

					String extension = "";
					String filename=f.getAbsolutePath();

					final int i = filename.lastIndexOf('.');
					if (i >= 0) 
					{
						extension = filename.substring(i+1);
						filename=filename.substring(0, i);
					}


					if(!extension.equals("bib"))
						extension="bib";

					outPath.setText(filename+"."+extension);
				}
			}
		});

		final JPanel okPanel = new JPanel();
		okPanel.setBackground(Color.WHITE);
		final FlowLayout flowLayout_1 = (FlowLayout) okPanel.getLayout();
		flowLayout_1.setAlignment(FlowLayout.RIGHT);
		flowLayout_1.setVgap(0);
		flowLayout_1.setHgap(0);
		footer.add(okPanel, BorderLayout.SOUTH);

		final JButton btnOk = new JButton("ok");
		btnOk.addActionListener(new ActionListener() {
			public void actionPerformed(final ActionEvent e) {
				OutputLogger.Instance().clear();
				OutputLogger.Instance().logMessage("Started merge");
				try {
					final JournalEntryDB journalDB = new JournalEntryDB();

					for( int i=0;i< listModelDB.getSize(); ++i) {   
						final String s=listModelDB.elementAt(i).toString();
						journalDB.addFromFile(s);
					}

					final ArrayList<BibDatabase> bibs=new ArrayList<BibDatabase>();
					final Set<EntryKey> entries=new HashSet<>();

					for( int i=0;i< listModel.getSize(); ++i) {   
						final String s=listModel.elementAt(i).toString();
						bibs.add(BibReader.ReadBibFile(s,journalDB,entries));
					}

					final EntriesSelection d=new EntriesSelection(BibMergerMain.this, entries);


					final BibDatabase merged=Merger.Merge(bibs,d.getEntries(),autogenerateIDS.isSelected(),bblPath.getText());
					journalDB.logMissingJournals();

					final File out = new File(outPath.getText());
					final PrintWriter pw = new PrintWriter(out);
					pw.write(merged.toString());
					pw.close();

					OutputLogger.Instance().logMessage("Merge finished! File saved on "+outPath.getText());
				} catch (final IOException e1) {
					OutputLogger.Instance().logError(e1.getMessage());
				}


				new OutputLogDialog(BibMergerMain.this);
			}
		});
		okPanel.add(btnOk);

		final JPanel bblPanel = new JPanel();
		bblPanel.setBackground(Color.WHITE);
		footer.add(bblPanel, BorderLayout.NORTH);

		final JLabel lblLoadBblFile = new JLabel("bbl file");
		bblPanel.add(lblLoadBblFile);

		bblPath = new JTextField();
		bblPanel.add(bblPath);
		bblPath.setColumns(20);

		final JButton browseBBL = new JButton("Browse...");
		browseBBL.addActionListener(new ActionListener() {
			private File lastFile=new File("");

			public void actionPerformed(final ActionEvent e) {
				final JFileChooser fc=new JFileChooser();
				fc.setCurrentDirectory(lastFile);
				final int res=fc.showOpenDialog(BibMergerMain.this);

				if(res==JFileChooser.APPROVE_OPTION){
					final File f=fc.getSelectedFile();

					bblPath.setText(f.getAbsolutePath());
					lastFile=f.getParentFile();
				}
			}
		});
		bblPanel.add(browseBBL);

		final GridBagLayout gbl_central = new GridBagLayout();
		gbl_central.columnWeights = new double[]{1.0};
		gbl_central.rowWeights = new double[]{1.0, 1.0};
		final JPanel central = new JPanel(gbl_central);
		contentPane.add(central, BorderLayout.CENTER);


		final JPanel bibs = new JPanel();
		bibs.setBorder(new LineBorder(new Color(0, 0, 0)));
		bibs.setBackground(Color.WHITE);
		final GridBagConstraints gbc_bibs = new GridBagConstraints();
		gbc_bibs.gridy = 0;
		gbc_bibs.gridx = 0;
		gbc_bibs.fill = GridBagConstraints.BOTH;
		central.add(bibs, gbc_bibs);
		bibs.setLayout(new BorderLayout(0, 0));

		final GridBagConstraints gbc_btns=new GridBagConstraints();
		gbc_btns.gridx=0; gbc_btns.gridy=0;
		final JPanel btns = new JPanel();
		//		btns.setBackground(Color.WHITE);
		bibs.add(btns,BorderLayout.SOUTH);

		final JButton btnAdd = new JButton("add");
		btnAdd.addActionListener(new ActionListener() {
			private File lastFile=new File("");


			public void actionPerformed(final ActionEvent arg0) {
				final JFileChooser fc=new JFileChooser();
				fc.setCurrentDirectory(lastFile);
				fc.setMultiSelectionEnabled(true);

				final int val=fc.showOpenDialog(BibMergerMain.this);

				if(val==JFileChooser.APPROVE_OPTION){
					final File[] files=fc.getSelectedFiles();
					for(final File f : files){
						listModel.addElement(f.getAbsolutePath());
						lastFile=f.getParentFile();
					}

				}

			}
		});
		btns.setLayout(new BorderLayout(0, 0));
		btns.add(btnAdd, BorderLayout.EAST);

		final JButton btnRemove = new JButton("remove");
		btnRemove.addActionListener(new ActionListener() {
			public void actionPerformed(final ActionEvent e) {
				final int index=inputPath.getSelectedIndex();
				if(index>=0) listModel.remove(index);
			}
		});
		btns.add(btnRemove, BorderLayout.WEST);

		listModel=new DefaultListModel<String>();
		inputPath = new JList<String>(listModel);
		inputPath.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		inputPath.setLayoutOrientation(JList.VERTICAL);

		boolean found=false;
		for(final String s : args){
			if(s.equals("-i")) 
				found=true;
			else if(s.startsWith("-")) 
				found=false;
			else if(found)
				listModel.addElement(s);
		}

		bibs.add(inputPath, BorderLayout.CENTER);

		final JLabel lblAddInputFiles = new JLabel("Bibliography files");
		//		lblAddInputFiles.setBackground(Color.WHITE);
		bibs.add(lblAddInputFiles, BorderLayout.NORTH);

		final JPanel bibsDB = new JPanel();
		bibsDB.setBorder(new LineBorder(new Color(0, 0, 0)));
		bibsDB.setBackground(Color.WHITE);
		final GridBagConstraints gbc_bibsDB = new GridBagConstraints();
		gbc_bibsDB.fill = GridBagConstraints.BOTH;
		gbc_bibsDB.gridy = 1;
		gbc_bibsDB.gridx = 0;
		central.add(bibsDB, gbc_bibsDB);
		bibsDB.setLayout(new BorderLayout(0, 0));




		listModelDB=new DefaultListModel<String>();
		final JList<String> inputPathDB = new JList<String>(listModelDB);
		inputPathDB.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		inputPathDB.setLayoutOrientation(JList.VERTICAL);



		found=false;
		for(final String s : args){
			if(s.equals("-db"))
				found=true;
			else if(s.startsWith("-")) 
				found=false;
			else if(found)
				listModelDB.addElement(s);
		}
		bibsDB.add(inputPathDB, BorderLayout.CENTER);

		found=false;
		for(final String s : args){
			if(s.equals("-bbl"))
				found=true;
			else if(found)
			{
				bblPath.setText(s);
				break;
			}

		}

		final JPanel btnsDB = new JPanel();
		//		btnsDB.setBackground(Color.WHITE);
		bibsDB.add(btnsDB, BorderLayout.SOUTH);
		btnsDB.setLayout(new BorderLayout(0, 0));



		final JButton addDB = new JButton("add");
		addDB.addActionListener(new ActionListener() {
			private File lastFile=new File("");

			public void actionPerformed(final ActionEvent arg0) {
				final JFileChooser fc=new JFileChooser();
				fc.setCurrentDirectory(lastFile);
				fc.setMultiSelectionEnabled(true);
				final int val=fc.showOpenDialog(BibMergerMain.this);

				if(val==JFileChooser.APPROVE_OPTION){
					final File[] files=fc.getSelectedFiles();
					for(final File f : files){
						listModelDB.addElement(f.getAbsolutePath());
						lastFile=f.getParentFile();
					}


				}

			}
		});
		btnsDB.add(addDB, BorderLayout.EAST);

		final JButton removeDB = new JButton("remove");
		removeDB.addActionListener(new ActionListener() {
			public void actionPerformed(final ActionEvent e) {
				final int index=inputPathDB.getSelectedIndex();
				if(index>=0) listModelDB.remove(index);
			}
		});
		btnsDB.add(removeDB, BorderLayout.WEST);

		final JLabel bibDBlbl = new JLabel("Journal databases");
		bibsDB.add(bibDBlbl, BorderLayout.NORTH);


	}

}
