package bibMerger;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;


public class BibEntry {
	private String key;
	private String type;
	private Map<EntryKey, String> content; 
	private boolean valid;
	private String stringContent;
	private Set<EntryKey> totalEntries;
	private List<EntryKey> toSkip;

	private static int MAX_SPACES=15;


	public BibEntry(final String text, final JournalEntryDB journalDB, final Set<EntryKey> entries)
	{	
		this.totalEntries=entries;
		valid=false;
		final String[] pieces=text.split("\n");

		if(pieces.length<=0) return;

		final String tmp=pieces[0].substring(0, pieces[0].trim().length()-1);
		final String[] firstLine=tmp.split("\\{");

		if(firstLine.length<=1) return;

		type=firstLine[0].trim();
		type=type.substring(0, 1).toUpperCase()+type.substring(1).toLowerCase();
		type="@"+type;

		key=firstLine[1].trim();

		content=new HashMap<>();
		buildModel(pieces);
		cleanModel(journalDB);


		valid=true;
	}

	private void buildModel(final String[] pieces)
	{
		String currentContent="", currentKey="";
		for(int i=1;i<pieces.length;++i){
			String text=pieces[i].trim();
			if(text.isEmpty() || (i==pieces.length-1 && text.equals("}")) ) continue;

			if(i==pieces.length-1 && text.endsWith("}}"))
				text=text.substring(0, text.length()-1);

			String[] tmp=text.split("=");

			if(tmp.length>2)
			{
				final String tmp0=tmp[0];
				String tmp1=tmp[1];

				for(int j=2;j<tmp.length;++j)
					tmp1+="="+tmp[j];

				tmp=new String[2];
				tmp[0]=tmp0;
				tmp[1]=tmp1;
			}

			if(tmp.length!=2) 
			{
				currentContent += " "+text.trim();
			}
			else
			{
				if(!currentKey.isEmpty())
					content.put(new EntryKey(currentKey), currentContent);

				currentKey = tmp[0].trim().toLowerCase();
				currentContent = tmp[1].trim();
			}
		}

		if(!currentKey.isEmpty() && !currentKey.equals("}"))
			content.put(new EntryKey(currentKey), currentContent);
	}

	private void cleanModel(final JournalEntryDB journalDB)
	{



		for(final EntryKey key : content.keySet()){
			totalEntries.add(new EntryKey(key.name));
			String value=content.get(key);

			value=value.replaceAll("\n", " ");

			if(value.endsWith(","))
				value=value.substring(0,value.length()-1).trim();

			if(key.name.equals("pages") || key.name.equals("number"))
			{

				if(value.contains("-") && !value.contains("--")){
					value=value.replaceAll("-", "--");
					value=value.replaceAll(" ", "");
				}
			}

			{
				if(value.startsWith("\""))
					value="{"+value.substring(1);
				if(value.endsWith("\""))
					value=value.substring(0,value.length()-1)+"}";
			}

			if(value.matches("\\{\\s*\\d+\\s*\\}"))
			{
				if(value.startsWith("{") || value.startsWith("\""))
					value=value.substring(1);
				if(value.endsWith("}") || value.endsWith("\""))
					value=value.substring(0,value.length()-1);
				value=value.replaceAll(" ", "");
			}




			if(key.name.equals("journal"))
			{
				final String abbr=journalDB.abbrFor(value.substring(1,value.length()-1));

				if(abbr==null)
				{
					if(journalDB.hasAbbr(value.toLowerCase()))
						value=value.toLowerCase();
					else{
						if(!value.startsWith("{"))
							value="{"+value+"}";

						journalDB.addMissingJournal(value);

					}
				}
				else
					value=abbr;
			}

			content.put(key, value);
		}

	}

	private void stringify()
	{
		final StringBuffer buf=new StringBuffer();

		final List<EntryKey> keys=new ArrayList<>(content.keySet());
		Collections.sort(keys);

		for(final EntryKey key : keys){
			final int index=toSkip.indexOf(key);

			if(index<0 || !toSkip.get(index).enabled)
				continue;

			final int nSpaces=MAX_SPACES-key.name.length();
			String spaces="";

			for(int i=0;i<nSpaces;++i) spaces+=" ";

			buf.append("   "+key.name+" "+spaces+" = "+content.get(key)+",\n");
		}

		if(buf.length()<=0)
		{
			for(final EntryKey key : keys){
				final int nSpaces=MAX_SPACES-key.name.length();
				String spaces="";

				for(int i=0;i<nSpaces;++i) spaces+=" ";

				buf.append("   "+key.name+" "+spaces+" = "+content.get(key)+",\n");
			}
		}

		stringContent=buf.substring(0, buf.length()-2);
	}

	public String toString()
	{
		if(valid){
			stringify();

			return type+"{"+key+",\n"+stringContent+"\n}\n";
		}

		return "";
	}

	public String getKey() {
		return key;
	}

	public void setEntriesToSkip(final List<EntryKey> toSkip) {
		this.toSkip=toSkip;
	}

	public void autogenerateID() {

		String author=content.get(new EntryKey("author")).trim();
		final String year=content.get(new EntryKey("year"));
		String title=content.get(new EntryKey("title"));

		String tmp[]=author.split(" ");

		for(final String s : tmp){
			if(s.length()>2){
				author=s.replaceAll("\\{", "").replaceAll("\\}", "").replaceAll(",", "").replaceAll("\\.", "").toLowerCase();
				break;
			}
		}

		tmp=title.split(" ");
		title="";
		int max=4;
		for(int i=0;i<max && i<tmp.length;++i){
			final String tmp1=tmp[i].replaceAll("\\{", "").replaceAll("\\}", "").replaceAll(",", "").replaceAll("\\.", "");
			if(!tmp1.isEmpty())
				title+=tmp1.toLowerCase().charAt(0);
			else
				++max;
		}

		key=author+":"+year+":"+title;
	}

}
