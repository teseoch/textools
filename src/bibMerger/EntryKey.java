package bibMerger;


public class EntryKey implements Comparable<EntryKey>
{
	public String name;
	public int priority;
	public boolean enabled;
	
	public EntryKey(final String name)
	{
		this.name=name;
		this.enabled=false;
		
		if(name.equals("author"))
			priority=0;
		else if(name.equals("title"))
			priority=1;
		else if(name.equals("booktitle"))
			priority=2;
		else if(name.equals("journal"))
			priority=3;
		else if(name.equals("volume"))
			priority=4;
		else if(name.equals("number"))
			priority=5;
		else if(name.equals("month"))
			priority=6;
		else if(name.equals("year"))
			priority=7;
		else if(name.equals("pages"))
			priority=8;
		else if(name.equals("editor"))
			priority=9;
		else if(name.equals("publisher"))
			priority=10;
		else
			priority=-1;
	}
	
	@Override
	public boolean equals(final Object other)
	{
		 if ( this == other ) return true;
		 if ( !(other instanceof EntryKey) ) return false;
		
		return name.equals(((EntryKey)other).name);
	}
	
	@Override
	public int hashCode() {
		return name.hashCode();
	}

	@Override
	public int compareTo(final EntryKey other) {
		if(priority < 0 && other.priority < 0)
			return name.compareTo(other.name);
		
		if(priority < 0)
			return 1;
		
		if(other.priority < 0)
			return -1;
		
		return priority-other.priority;
	}
}