package bibMerger;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Frame;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JScrollPane;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextPane;
import javax.swing.JTextArea;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;

public class AboutDialog extends JDialog {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private final JPanel contentPanel = new JPanel();


	/**
	 * Create the dialog.
	 */
	public AboutDialog(final Frame parent) {
		super(parent);

		setBounds(100, 100, 450, 300);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		{
			final JPanel main=new JPanel();
			final JScrollPane scrollPane = new JScrollPane(main);
			final GridBagLayout gbl_main = new GridBagLayout();
			gbl_main.columnWidths = new int[] {450};
			gbl_main.rowHeights = new int[] {0};
			gbl_main.columnWeights = new double[]{0.0};
			gbl_main.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0};
			main.setLayout(gbl_main);
			{
				final JLabel lblNewLabel = new JLabel("Bibliography files");
				lblNewLabel.setFont(new Font("Lucida Grande", Font.PLAIN, 16));
				final GridBagConstraints gbc_lblNewLabel = new GridBagConstraints();
				gbc_lblNewLabel.fill = GridBagConstraints.BOTH;
				gbc_lblNewLabel.insets = new Insets(0, 0, 5, 0);
				gbc_lblNewLabel.gridx = 0;
				gbc_lblNewLabel.gridy = 0;
				main.add(lblNewLabel, gbc_lblNewLabel);
			}
			{
				final JTextPane txtpnAddInThis = new JTextPane();
				txtpnAddInThis.setEditable(false);
				txtpnAddInThis.setText("Add in this list all your bib file.\nThe tool supports only bib entries.");
				final GridBagConstraints gbc_txtpnAddInThis = new GridBagConstraints();
				gbc_txtpnAddInThis.fill = GridBagConstraints.BOTH;
				gbc_txtpnAddInThis.insets = new Insets(0, 0, 5, 0);
				gbc_txtpnAddInThis.gridx = 0;
				gbc_txtpnAddInThis.gridy = 1;
				main.add(txtpnAddInThis, gbc_txtpnAddInThis);
			}
			{
				final JLabel lblJournalDatabases = new JLabel("Journal databases");
				lblJournalDatabases.setFont(new Font("Lucida Grande", Font.PLAIN, 16));
				final GridBagConstraints gbc_lblJournalDatabases = new GridBagConstraints();
				gbc_lblJournalDatabases.fill = GridBagConstraints.BOTH;
				gbc_lblJournalDatabases.insets = new Insets(0, 0, 5, 0);
				gbc_lblJournalDatabases.gridx = 0;
				gbc_lblJournalDatabases.gridy = 2;
				main.add(lblJournalDatabases, gbc_lblJournalDatabases);
			}
			{
				final JTextArea txtrAddTheJournal_1 = new JTextArea();
				txtrAddTheJournal_1.setEditable(false);
				txtrAddTheJournal_1.setText("Add the journal databases.\nThese files contains the journal acronym \nfor both short and long names.");
				final GridBagConstraints gbc_txtrAddTheJournal_1 = new GridBagConstraints();
				gbc_txtrAddTheJournal_1.fill = GridBagConstraints.BOTH;
				gbc_txtrAddTheJournal_1.insets = new Insets(0, 0, 5, 0);
				gbc_txtrAddTheJournal_1.gridx = 0;
				gbc_txtrAddTheJournal_1.gridy = 3;
				main.add(txtrAddTheJournal_1, gbc_txtrAddTheJournal_1);
			}
			{
				final JLabel lblBblFile = new JLabel("bbl file");
				lblBblFile.setFont(new Font("Lucida Grande", Font.PLAIN, 16));
				final GridBagConstraints gbc_lblBblFile = new GridBagConstraints();
				gbc_lblBblFile.fill = GridBagConstraints.BOTH;
				gbc_lblBblFile.insets = new Insets(0, 0, 5, 0);
				gbc_lblBblFile.gridx = 0;
				gbc_lblBblFile.gridy = 4;
				main.add(lblBblFile, gbc_lblBblFile);
			}
			{
				final JTextPane txtpnSelectABbl = new JTextPane();
				txtpnSelectABbl.setText("Select a bbl file to filter unreferenced references");
				final GridBagConstraints gbc_txtpnSelectABbl = new GridBagConstraints();
				gbc_txtpnSelectABbl.fill = GridBagConstraints.BOTH;
				gbc_txtpnSelectABbl.insets = new Insets(0, 0, 5, 0);
				gbc_txtpnSelectABbl.gridx = 0;
				gbc_txtpnSelectABbl.gridy = 5;
				main.add(txtpnSelectABbl, gbc_txtpnSelectABbl);
			}
			{
				final JLabel lblAutogenerateIds = new JLabel("Autogenerate ids");
				lblAutogenerateIds.setFont(new Font("Lucida Grande", Font.PLAIN, 16));
				final GridBagConstraints gbc_lblAutogenerateIds = new GridBagConstraints();
				gbc_lblAutogenerateIds.fill = GridBagConstraints.BOTH;
				gbc_lblAutogenerateIds.insets = new Insets(0, 0, 5, 0);
				gbc_lblAutogenerateIds.gridx = 0;
				gbc_lblAutogenerateIds.gridy = 6;
				main.add(lblAutogenerateIds, gbc_lblAutogenerateIds);
			}
			{
				final JTextPane txtpnSelectToRegenerate = new JTextPane();
				txtpnSelectToRegenerate.setText("Select to regenerate the bib entries key.\nWarning, this option does not change the tex files.");
				final GridBagConstraints gbc_txtpnSelectToRegenerate = new GridBagConstraints();
				gbc_txtpnSelectToRegenerate.fill = GridBagConstraints.BOTH;
				gbc_txtpnSelectToRegenerate.insets = new Insets(0, 0, 5, 0);
				gbc_txtpnSelectToRegenerate.gridx = 0;
				gbc_txtpnSelectToRegenerate.gridy = 7;
				main.add(txtpnSelectToRegenerate, gbc_txtpnSelectToRegenerate);
			}
			{
				final JLabel lblOutput = new JLabel("Output");
				lblOutput.setFont(new Font("Lucida Grande", Font.PLAIN, 16));
				final GridBagConstraints gbc_lblOutput = new GridBagConstraints();
				gbc_lblOutput.fill = GridBagConstraints.BOTH;
				gbc_lblOutput.insets = new Insets(0, 0, 5, 0);
				gbc_lblOutput.gridx = 0;
				gbc_lblOutput.gridy = 8;
				main.add(lblOutput, gbc_lblOutput);
			}
			{
				final JTextPane txtpnChooseWhereTo = new JTextPane();
				txtpnChooseWhereTo.setText("Choose where to save the generated bib file.\nWarning: do not override the original files.");
				final GridBagConstraints gbc_txtpnChooseWhereTo = new GridBagConstraints();
				gbc_txtpnChooseWhereTo.fill = GridBagConstraints.BOTH;
				gbc_txtpnChooseWhereTo.gridx = 0;
				gbc_txtpnChooseWhereTo.gridy = 9;
				main.add(txtpnChooseWhereTo, gbc_txtpnChooseWhereTo);
			}
			contentPanel.setLayout(new BorderLayout(0, 0));
			contentPanel.add(scrollPane);
		}
		{
			final JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				final JButton okButton = new JButton("OK");
				okButton.addActionListener(new ActionListener() {
					public void actionPerformed(final ActionEvent e) {
						setVisible(false);
					}
				});
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
		}
		

		setModal(true);
		setVisible(true);
	}

}
